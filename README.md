# endpoint coding challenge
## description
The challenge was to create a program that would read input and perform a series of commands that would produce output matching the expected sample. Usage of 3rd party was discouraged. I did use one function from SO and I added the link to the answer.
## installation
If you're just running the script, no instalation is needed. However, if you're going to be developing on top of this or running the tests, you'll need to run
`npm install`
## execution
you can run the program and provide arguments separated by a space
`node directories.js [arg arg arg]`

* provide contents via file: `node directories.js my-file.txt`
* use the provided input: `npm start`
* provide the contents from the cli: `node directories.js "CREATE milk\nCREATE bread\nCREATE eggs\nLIST" `
  * **this was not tested**


### options
* **first argument:** you can provide a custom path for your directories. if nothing is provided, the system will use the default
* **second argument:** you cab provide the instructions
* **third argument:** you can choose your custom indentation character. A double space is used by default. If choosing an special character, depending on the platform this script is ran, you'll need to escape them or pass them within quotes.
## tests
you can execute the tests by running
`npm t` or `npm run test`
