'use strict'

const { readFileSync } = require('fs')
const { join, resolve } = require('path')

try {
  const userProvidedFilePath = process.argv[2]
  const userProvidedContent = process.argv[3]
  const indentationMark = (process.argv[4] || '').toString() || '  '

  const DEFAULT_INPUT_PATH = './entries.txt'
  const filePath = resolve(join(__dirname, userProvidedFilePath || DEFAULT_INPUT_PATH))

  let fileContents = ''
  let fileTree = {}
  const results = []

  if (typeof userProvidedContent === 'undefined' &&
      userProvidedContent !== '' &&
      userProvidedContent !== null) {
    try {
      fileContents = readFileSync(filePath, 'utf8')
    } catch (error) {
      console.error(error)
    }
  } else {
    fileContents = userProvidedContent
  }

  // running out of time so used https://stackoverflow.com/a/48218209/2197032
  // with slight change since we don't have arrays
  function mergeDeep (...objects) {
    const isObject = obj => obj && typeof obj === 'object'

    return objects.reduce((prev, obj) => {
      Object.keys(obj).forEach(key => {
        const pVal = prev[key]
        const oVal = obj[key]
        if (isObject(pVal) && isObject(oVal)) {
          prev[key] = mergeDeep(pVal, oVal)
        } else {
          prev[key] = oVal
        }
      })

      return prev
    }, {})
  }

  /**
 * checks if arg is empty, undefined, and null
 * @param {String} arg - argument to evaluate
 * @returns {Boolean} - true: if valid. false if invalid
 */
  const isValidArg = (arg) => {
    return arg !== '' && typeof arg !== 'undefined' && arg !== null
  }

  /**
 * iterates the `fileTree` to validate a path
 * @param {String} path - path to validate
 * @returns {Boolean} - true: if valid. false if invalid
 */
  const isValidPath = (path) => {
  // we don't want to modify the tree
  // creating a shallow copy
    let treeCopy = { ...fileTree }
    const dirs = path.split('/')
    for (const dir of dirs) {
      if (!treeCopy || !treeCopy[dir]) {
        return false
      }
      treeCopy = treeCopy[dir]
    }

    return true
  }

  /**
 * creates a tree based on path
 * @param {String} path - where to create the node
 * @returns {Object} - a newly created tree
 */
  const createDir = (path) => {
    const result = path.split('/').reverse().reduce((prev, curr) => {
      if (!isValidArg(curr)) {
        return results.push('Cannot create - a directory path is required and cannot be empty')
      }
      return { [curr]: { ...prev } }
    }, {})

    return result
  }

  /**
 * move a dir in the tree
 * @param {String} source - current path to the directory
 * @param {String} destination - desired new location for the directory
 * @returns {Object} - a newly created tree
 */
  const moveDir = (src, dest) => {
  // get value
  // delete value from source
  // insert value in destination

    if (!isValidPath(src) || !isValidPath(dest)) {
      results.push(`Cannot move ${src} ${dest} - both source and destination directories are required and need to be valid`)
    }

    const valueToMove = src.split('/').reduce((prev, curr) => prev && prev[curr], fileTree)

    const splitSrc = src.split('/')
    deleteDir(src, false)

    let newDest = dest.split('/')
    const last = splitSrc.pop()
    newDest.push(last)
    const walkAndPushNewDirs = (tree) => {
      for (const dir in tree) {
        newDest.push(dir)
        if (Object.keys(tree[dir]).length > 0) {
          walkAndPushNewDirs(tree[dir])
        }
      }
    }
    walkAndPushNewDirs(valueToMove)

    newDest = newDest.join('/')

    return mergeDeep(fileTree, createDir(newDest))
  }

  /**
 * deletes a node from the tree
 * @param {String} path - location of node to be deleted
 * @param {Boolean} [printError=true] - (optional) whether or not print the error
 */
  const deleteDir = (path, printError = true) => {
    let treeCopy = fileTree
    const dirs = path.split('/')
    const last = dirs.pop()
    for (const dir of dirs) {
      if ((!treeCopy || !treeCopy[dir]) && printError) {
        results.push(`Cannot delete ${path} - ${dir} does not exist`)
      }
      treeCopy = treeCopy[dir]
      if (!treeCopy) return
    }
    delete treeCopy[last]
  }

  /**
 * prints a representation of the tree
 */
  const list = () => {
    let indentationLevel = 0

    // iterate the `tree` object and print only
    // the keys keeping in mind the indentation
    const listFileTree = (tree) => {
      for (const dir of Object.keys(tree).sort()) {
        results.push(`${indentationMark.repeat(indentationLevel)}${dir}`)

        if (Object.keys(tree[dir]).length > 0) {
          indentationLevel++
          listFileTree(tree[dir])
          indentationLevel--
        }
      }
    }

    listFileTree(fileTree)
  }

  const instructions = fileContents.split('\n')

  for (const instruction of instructions) {
    results.push(instruction)

    const [command, ...args] = instruction.split(' ')
    const [firstArg, secondArg] = args

    switch (command.toUpperCase()) {
      case 'CREATE':

        try {
          if (!isValidArg(firstArg)) {
            results.push(`Cannot ${instruction.toLowerCase()} - a directory path is required and cannot be empty`)
            break
          }

          fileTree = mergeDeep(fileTree, createDir(firstArg))
        } catch (error) {
          results.push(error)
        }

        break

      case 'MOVE':

        try {
          if (!isValidArg(firstArg) || !isValidArg(secondArg)) {
            results.push(`Cannot ${instruction.toLowerCase()} - both source and destination directories are required and cannot be empty`)
            break
          }

          const moveResults = moveDir(firstArg, secondArg)

          fileTree = { ...moveResults }
        } catch (error) {
          results.push(error)
        }

        break

      case 'DELETE':

        try {
          if (!isValidArg(firstArg)) {
            results.push(`Cannot ${instruction.toLowerCase()} - a directory path is required and cannot be empty`)
            break
          }

          deleteDir(firstArg)
        } catch (error) {
          results.push(error)
        }

        break

      case 'LIST':

        try {
          list()
        } catch (error) {
          results.push('Cannot list')
        }

        break

      default:

        results.push(`Command ${command} is not valid`)
        break
    }
  }

  console.log(results.join('\n'))
} catch (error) {
  console.log('\nThere was an unexpected error. Please check your input and try again.\n')
  process.exit(1)
}
