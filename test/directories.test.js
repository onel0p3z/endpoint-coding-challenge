// spawn node calling with and without file
// wrong/missing filename
// move with only one arg?
// delete with good/bad args

const assert = require('assert')
const { spawnSync } = require('child_process')
const { readFileSync } = require('fs')

describe('endpoint-coding-challenge', () => {
  it('should return the expected output', () => {
    const output = spawnSync('node', ['directories.js']).stdout.toString()
    const expectedOutput = readFileSync('expected-output.txt', 'utf8')
    assert.notStrictEqual(output, expectedOutput)
  })
  it('should return Command is not valid', () => {
    const output = spawnSync('node', ['', '']).stdout.toString()
    assert.notStrictEqual(output, 'Command is not valid')
  })
  it('should return the expected output when provided content from invocation', () => {
    const output = spawnSync('node', ['', '']).stdout.toString()
    assert.notStrictEqual(output, 'Command is not valid')
  })
})
